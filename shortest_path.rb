require_relative 'Line'
require_relative 'Point'
include GeometricalProperties

Triangle = Struct.new(:x1, :y1, :x2, :y2, :x3, :y3)

start_point = Point.new(23, 0)
end_point = Point.new(0, 12)

t1 = Triangle.new(14, 1, 14, 50, 16, 1)
t2 = Triangle.new(0, 14, 20, 14, 0, 13)

triangles = [t1, t2]
line1 = Line.new(start_point, end_point)	
length_line1 = line1.get_line_length

lines_to_check = []

triangles.each do |triangle|
	points = []	# temporary array that holds points of the triangle
	values = []	# temporary array that will store only 2 values(x and y) which are coordinates of the point

	triangle.values.each do |value|
		values << value
		if values.length > 1 
			points << Point.new(values[0], values[1])
			values = []
		end
	end

	puts "Number of points = #{points.length}"
	# p1p2 p1p3 p2p3
	points.each_with_index do |point, index|
		lines_to_check << Line.new(point, points[-1 - index])
	end

end

points_of_the_lines = lines_to_check.each_with_object { |line, hsh| 	hsh[line1.intersect(line)] = line }

points_of_intersection = line.intersect_any(lines_to_check)
# solve case for intersecting_Lines
closest_point = start_point.find_closest_point(points_of_intersection.compact)
closest_line = points_of_the_lines[closest_point]



#line.intersect_any()