module GeometricalProperties	
	class Point
		attr_reader :x, :y

		def initialize(x, y)
			@x = x
			@y = y
		end

		def distance_to(point)
			square_sum = ((@x - point.x) ** 2) + ((@y - point.y) ** 2)
			Math.sqrt(square_sum)
		end

		# Find the nearest point in the given set of points
		def find_closest_point(points)
			points.map {|point| distance_to(point)}.min
		end
	end
end