module GeometricalProperties
	class Line
		attr_reader :a, :b, :slope
 
	  def initialize(point1, point2)
	    @a = (point1.y - point2.y).fdiv(point1.x - point2.x)
	    @b = point1.y - @a*point1.x
	    @start_point 	= point1
	    @end_point 		= point2
	  end

		# Printing human-readable equation of the line
		def get_line_equation
			slope = calculate_line_slope(@start_point, @end_point)
			if @start_point.x == @end_point.x
				return slope
			end

			free_member = slope * -@end_point.x
			equation_string = "y = #{slope}x"
			
			if free_member != 0
				if free_member > 0
					return "#{equation_string}+#{free_member}"
				end
				"#{equation_string}#{free_member}"
			end
		end

		# Checking if selected Point is the part of the Line
		def point_on_the_line(point)
			x_coords = [@start_point.x, @end_point.x]
			y_coords = [@start_point.y, @end_point.y]

			line_slope 									= calculate_line_slope(@start_point, @end_point)
			unchecked_point_line_slope 	= calculate_line_slope(@start_point, point)

			if point.equal?(@start_point) or point.equal?(@end_point)
				return true
			end

			if point.x.between?(x_coords.min, x_coords.max) and point.y.between?(y_coords.min, y_coords.max)
				# points which should theoretically be on the line, were calculated as they weren't because they weren't rounded
				# In order not to loose potentially important decimal digits, condition that given slope should be less than 1/1000 different than original line's slope
				# to be considered as the same one
				if (line_slope - unchecked_point_line_slope).abs < 1e-03
					return true
				end    
			end

			return false
		end

		# Checking if other Line is intersecting with Line instance selected for comparision
		def intersect(other)
			return nil if @a == other.a
			x = (other.b - @b).fdiv(@a - other.a)
			y = @a*x + @b
			point = Point.new(x, y)

			# This check is added because this is checking if intersection happens within the boundaries of Line constructor points
			if (point_on_the_line(point))
				return point
			else
				return nil
			end
		end

		def intersect_any(lines)
			intersecting_lines = []

			lines.each do |line|
				intersecting_lines << intersect(line)
			end

			return intersecting_lines
		end 

		private

		def calculate_line_slope(first_point, second_point)
			if second_point.x != first_point.x	
				@slope = (second_point.y - first_point.y) / (second_point.x - first_point.x).to_f
			else
				puts "x=#{second_point.x}"
			end
		end
	end
end